# TEST_binder_jupyter-book

binder von GitLab aus und jupyter book auf gitlab pages.

## Link zum binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/AndreGismo%2Ftest_binder_jupyter-book/HEAD)

## Link zum jupyter book

[Hier klicken](https://andregismo.gitlab.io/test_binder_jupyter-book)
